package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.Car;
import com.epam.lab.jdbc.transformer.CarTransformer;

import java.sql.*;
import java.util.List;

public class CarDao implements GenericDao<Car, Integer> {
    private static final String SELECT_ALL_FROM_CAR_QUERY = "SELECT * FROM car";
    private static final String SELECT_ALL_FROM_CAR_BY_ID_QUERY = "SELECT * FROM car WHERE id = ?";
    private static final String INSERT_INTO_CAR_QUERY = "INSERT INTO car(model,number,color) VALUES (?,?,?)";
    private static final String UPDATE_CAR_QUERY = "UPDATE car SET model=?,number=?,color=? WHERE id=?";
    private static final String DELETE_FROM_CAR_BY_ID_QUERY = "DELETE FROM car WHERE id=?";
    private Connection connection;
    private CarTransformer transformer;

    public CarDao(Connection connection) {
        this.connection = connection;
        this.transformer = new CarTransformer();
    }

    @Override
    public List<Car> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_CAR_QUERY);
        List<Car> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public Car getByPK(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_FROM_CAR_BY_ID_QUERY);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        Car ret = transformer.transformResultSetToObject(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public int create(Car obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_CAR_QUERY);
        statement.setString(1, obj.getModel());
        statement.setString(2, obj.getNumber());
        statement.setString(3, obj.getColor());
        return statement.executeUpdate();
    }

    @Override
    public void update(Car obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_CAR_QUERY);
        statement.setString(1, obj.getModel());
        statement.setString(2, obj.getNumber());
        statement.setString(3, obj.getColor());
        statement.setInt(4, obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_CAR_BY_ID_QUERY);
        statement.setInt(1, id);
        statement.execute();
    }
}
