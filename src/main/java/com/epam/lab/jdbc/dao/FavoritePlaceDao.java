package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.FavoritePlace;
import com.epam.lab.jdbc.transformer.FavoritePlaceTransformer;

import java.sql.*;
import java.util.List;

public class FavoritePlaceDao implements GenericDao<FavoritePlace, Integer> {
    private static final String SELECT_ALL_FROM_FAVORITE_PLACE_QUERY = "SELECT * FROM favorite_place";
    private static final String SELECT_ALL_FROM_FAVORITE_PLACE_BY_ID_QUERY = "SELECT * FROM favorite_place WHERE id = ?";
    private static final String INSERT_INTO_FAVORITE_PLACE_QUERY = "INSERT INTO favorite_place(id,name,user_id,location_id) VALUES (?,?,?,?)";
    private static final String UPDATE_FAVORITE_PLACE_QUERY = "UPDATE favorite_place SET name=?,user_id=?,location_id=? WHERE id=?";
    private static final String DELETE_FROM_FAVORITE_PLACE_BY_ID_QUERY = "DELETE FROM favorite_place WHERE id=?";
    private Connection connection;
    private FavoritePlaceTransformer transformer;

    public FavoritePlaceDao(Connection connection) {
        this.connection = connection;
        this.transformer = new FavoritePlaceTransformer();
    }

    @Override
    public List<FavoritePlace> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_FAVORITE_PLACE_QUERY);
        List<FavoritePlace> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public FavoritePlace getByPK(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_FROM_FAVORITE_PLACE_BY_ID_QUERY);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        FavoritePlace ret = transformer.transformResultSetToObject(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public int create(FavoritePlace obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_FAVORITE_PLACE_QUERY);
        statement.setInt(1, obj.getId());
        statement.setString(2, obj.getName());
        statement.setInt(3, obj.getUserId());
        statement.setInt(4, obj.getLocationId());
        return statement.executeUpdate();
    }

    @Override
    public void update(FavoritePlace obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                UPDATE_FAVORITE_PLACE_QUERY);
        statement.setString(1, obj.getName());
        statement.setInt(2, obj.getUserId());
        statement.setInt(3, obj.getLocationId());
        statement.setInt(4,obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_FAVORITE_PLACE_BY_ID_QUERY);
        statement.setInt(1, id);
        statement.execute();
    }
}
