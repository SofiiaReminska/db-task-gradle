package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.User;
import com.epam.lab.jdbc.transformer.UserTransformer;

import java.sql.*;
import java.util.List;

public class UserDao implements GenericDao<User, Integer> {
    private static final String SELECT_ALL_FROM_USER_QUERY = "SELECT * FROM user";
    private static final String SELECT_ALL_FROM_USER_BY_ID_QUERY = "SELECT * FROM user WHERE id = ?";
    private static final String INSERT_INTO_USER_QUERY = "INSERT INTO user(name,surname,email,password,phone,rate) VALUES (?,?,?,?,?,?)";
    private static final String UPDATE_USER_QUERY = "UPDATE user SET name=?,surname=?,email=?,password=?,phone=?,rate=? WHERE id=?";
    private static final String DELETE_FROM_USER_BY_ID_QUERY = "DELETE FROM user WHERE id=?";
    private Connection connection;
    private UserTransformer transformer;

    public UserDao(Connection connection) {
        this.connection = connection;
        this.transformer = new UserTransformer();
    }

    @Override
    public List<User> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_USER_QUERY);
        List<User> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public User getByPK(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_FROM_USER_BY_ID_QUERY);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        User ret = transformer.transformResultSetToObject(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public int create(User obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_USER_QUERY);
        statement.setString(1, obj.getName());
        statement.setString(2, obj.getSurname());
        statement.setString(3, obj.getEmail());
        statement.setString(4, obj.getPassword());
        statement.setString(5, obj.getPhone());
        statement.setObject(6, obj.getRate());
        return statement.executeUpdate();
    }

    @Override
    public void update(User obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                UPDATE_USER_QUERY);
        statement.setString(1, obj.getName());
        statement.setString(2, obj.getSurname());
        statement.setString(3, obj.getEmail());
        statement.setString(4, obj.getPassword());
        statement.setString(5,obj.getPhone());
        statement.setDouble(6, obj.getRate());
        statement.setInt(7, obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_USER_BY_ID_QUERY);
        statement.setInt(1, id);
        statement.execute();
    }
}
