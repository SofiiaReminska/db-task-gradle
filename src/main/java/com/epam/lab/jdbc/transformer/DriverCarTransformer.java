package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.DriverCar;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class DriverCarTransformer implements GenericTransformer<DriverCar> {
    @Override
    public List<DriverCar> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<DriverCar> ret = new ArrayList<DriverCar>();

        while (rs.next()) {
            Integer driverId = rs.getInt(DRIVER_ID);
            Integer carId = rs.getInt(CAR_ID);
            ret.add(new DriverCar(driverId, carId));
        }
        return ret;
    }

    @Override
    public DriverCar transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer driverId = rs.getInt(DRIVER_ID);
        Integer carId = rs.getInt(CAR_ID);
        return new DriverCar(driverId, carId);
    }
}
