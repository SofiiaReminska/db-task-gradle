package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.FavoritePlace;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class FavoritePlaceTransformer implements GenericTransformer<FavoritePlace> {
    @Override
    public List<FavoritePlace> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<FavoritePlace> ret = new ArrayList<FavoritePlace>();
        while (rs.next()) {
            Integer id = rs.getInt(FAVORITE_PLACE_ID);
            String name = rs.getString(FAVORITE_PLACE_NAME);
            Integer user_id = rs.getInt(FAVORITE_PLACE_USER_ID);
            Integer location_id = rs.getInt(FAVORITE_PLACE_LOCATION_ID);
            ret.add(new FavoritePlace(id, name, user_id, location_id));
        }
        return ret;
    }

    @Override
    public FavoritePlace transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer id = rs.getInt(FAVORITE_PLACE_ID);
        String name = rs.getString(FAVORITE_PLACE_NAME);
        Integer user_id = rs.getInt(FAVORITE_PLACE_USER_ID);
        Integer location_id = rs.getInt(FAVORITE_PLACE_LOCATION_ID);
        return new FavoritePlace(id, name, user_id, location_id);
    }
}
