package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.Driver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class DriverTransformer implements GenericTransformer<Driver> {
    @Override
    public List<Driver> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<Driver> ret = new ArrayList<Driver>();

        while (rs.next()) {
            Integer id = rs.getInt(DRIVER_ID);
            String name = rs.getString(DRIVER_NAME);
            String surname = rs.getString(DRIVER_SURNAME);
            String email = rs.getString(DRIVER_EMAIL);
            String password = rs.getString(DRIVER_PASSWORD);
            String phone = rs.getString(DRIVER_PHONE);
            Double rate = rs.getDouble(DRIVER_RATE);

            ret.add(new Driver(id, name, surname, email, password, phone, rate));
        }
        return ret;
    }

    @Override
    public Driver transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer id = rs.getInt(DRIVER_ID);
        String name = rs.getString(DRIVER_NAME);
        String surname = rs.getString(DRIVER_SURNAME);
        String email = rs.getString(DRIVER_EMAIL);
        String password = rs.getString(DRIVER_PASSWORD);
        String phone = rs.getString(DRIVER_PHONE);
        Double rate = rs.getDouble(DRIVER_RATE);

        return new Driver(id, name, surname, email, password, phone, rate);
    }
}

