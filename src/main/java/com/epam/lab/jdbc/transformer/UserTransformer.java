package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class UserTransformer implements GenericTransformer<User> {
    @Override
    public List<User> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<User> ret = new ArrayList<User>();

        while (rs.next()) {
            Integer id = rs.getInt(USER_ID);
            String name = rs.getString(USER_NAME);
            String surname = rs.getString(USER_SURNAME);
            String email = rs.getString(USER_EMAIL);
            String password = rs.getString(USER_PASSWORD);
            String phone = rs.getString(USER_PHONE);
            Double rate = rs.getDouble(USER_RATE);
            ret.add(new User.UserBuilder().setId(id).setName(name).setSurname(surname).setEmail(email).setPassword(password).setPhone(phone).setRate(rate).build());
        }
        return ret;
    }

    @Override
    public User transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer id = rs.getInt(USER_ID);
        String name = rs.getString(USER_NAME);
        String surname = rs.getString(USER_SURNAME);
        String email = rs.getString(USER_EMAIL);
        String password = rs.getString(USER_PASSWORD);
        String phone = rs.getString(USER_PHONE);
        Double rate = rs.getDouble(USER_RATE);
        return new User.UserBuilder().setId(id).setName(name).setSurname(surname).setEmail(email).setPassword(password).setPhone(phone).setRate(rate).build();
    }
}
