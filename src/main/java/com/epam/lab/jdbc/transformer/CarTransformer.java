package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.Car;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class CarTransformer implements GenericTransformer<Car> {

    @Override
    public List<Car> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<Car> ret = new ArrayList<Car>();

        while (rs.next()) {
            Integer id = rs.getInt(CAR_ID);
            String model = rs.getString(CAR_MODEL);
            String number = rs.getString(CAR_NUMBER);
            String color = rs.getString(CAR_COLOR);
            ret.add(new Car(id, model, number, color));
        }
        return ret;
    }

    @Override
    public Car transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer id = rs.getInt(CAR_ID);
        String model = rs.getString(CAR_MODEL);
        String number = rs.getString(CAR_NUMBER);
        String color = rs.getString(CAR_COLOR);
        return new Car(id, model, number, color);
    }
}
