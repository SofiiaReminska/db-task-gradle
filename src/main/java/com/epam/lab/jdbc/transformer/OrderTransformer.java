package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.Order;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class OrderTransformer implements GenericTransformer<Order> {
    @Override
    public List<Order> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<Order> ret = new ArrayList<Order>();

        while (rs.next()) {
            Integer id = rs.getInt(ORDER_ID);
            Double driverRate = rs.getDouble(ORDER_DRIVER_RATE);
            Double distance = rs.getDouble(ORDER_DISTANCE);
            Timestamp datetime = rs.getTimestamp(ORDER_DATETIME);
            BigDecimal price = rs.getBigDecimal(ORDER_PRICE);
            Integer userId = rs.getInt(USER_ID);
            Integer driverId = rs.getInt(DRIVER_ID);
            Integer departureLocationId = rs.getInt(ORDER_DEPARTURE_LOCATION_ID);
            Integer destinationLocationId = rs.getInt(ORDER_DESTINATION_LOCATION_ID);
            Integer carId = rs.getInt(CAR_ID);
            ret.add(new Order(id, driverRate, distance, datetime, price, userId, driverId,
                    departureLocationId, destinationLocationId, carId));
        }
        return ret;
    }

    @Override
    public Order transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer id = rs.getInt(ORDER_ID);
        Double driverRate = rs.getDouble(ORDER_DRIVER_RATE);
        Double distance = rs.getDouble(ORDER_DISTANCE);
        Timestamp datetime = rs.getTimestamp(ORDER_DATETIME);
        BigDecimal price = rs.getBigDecimal(ORDER_PRICE);
        Integer userId = rs.getInt(USER_ID);
        Integer driverId = rs.getInt(DRIVER_ID);
        Integer departureLocationId = rs.getInt(ORDER_DEPARTURE_LOCATION_ID);
        Integer destinationLocationId = rs.getInt(ORDER_DESTINATION_LOCATION_ID);
        Integer carId = rs.getInt(CAR_ID);
        return new Order(id, driverRate, distance, datetime, price, userId, driverId,
                departureLocationId, destinationLocationId, carId);
    }
}
