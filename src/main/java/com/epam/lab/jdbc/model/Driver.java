package com.epam.lab.jdbc.model;

public class Driver {
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
    private Double rate;

    public Driver(Integer id, String name, String surname, String email, String password, String phone, Double rate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.rate = rate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", rate=" + rate +
                '}';
    }

    public static class DriverBuilder {
        private Integer id;
        private String name;
        private String surname;
        private String email;
        private String password;
        private String phone;
        private Double rate;

        public DriverBuilder setId(Integer id) {
            this.id = id;
            return this;
        }

        public DriverBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public DriverBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public DriverBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public DriverBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public DriverBuilder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public DriverBuilder setRate(Double rate) {
            this.rate = rate;
            return this;
        }

        public Driver build() {
            return new Driver(id, name, surname, email, password, phone, rate);
        }
    }
}
