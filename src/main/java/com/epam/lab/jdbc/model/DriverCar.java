package com.epam.lab.jdbc.model;

public class DriverCar {
    private Integer driverId;
    private Integer carId;

    public DriverCar() {
    }

    public DriverCar(Integer driverId, Integer carId) {
        this.driverId = driverId;
        this.carId = carId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    @Override
    public String toString() {
        return "DriverCar{" +
                "driverId=" + driverId +
                ", carId=" + carId +
                '}';
    }
}
