package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.dao.DriverDao;
import com.epam.lab.jdbc.model.Driver;
import com.epam.lab.jdbc.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class DriverService {
    private static final Logger LOGGER = LogManager.getLogger(DriverService.class);
    private DriverDao driverDao = new DriverDao(ConnectionManager.getConnection());

    public void createSampleDriver() throws SQLException {

        try {
            driverDao.create(new Driver.DriverBuilder()
                    .setName("Ivan")
                    .setSurname("Ivanko")
                    .setPassword("12345")
                    .setPhone("0998989898")
                    .setEmail("ivan@gmail.com")
                    .build());
        } catch (SQLException e) {
            LOGGER.error("Cannot create driver ", e);
        }
        LOGGER.info(driverDao.getAll());
    }
}
