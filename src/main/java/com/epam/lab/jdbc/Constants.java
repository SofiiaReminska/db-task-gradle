package com.epam.lab.jdbc;

public class Constants {
    public static final String CAR_ID = "id";
    public static final String CAR_MODEL = "model";
    public static final String CAR_NUMBER = "number";
    public static final String CAR_COLOR = "color";
    public static final String DRIVER_ID = "id";
    public static final String DRIVER_NAME = "name";
    public static final String DRIVER_SURNAME = "surname";
    public static final String DRIVER_EMAIL = "email";
    public static final String DRIVER_PASSWORD = "password";
    public static final String DRIVER_PHONE = "phone";
    public static final String DRIVER_RATE = "rate";
    public static final String USER_ID = "id";
    public static final String USER_NAME = "name";
    public static final String USER_SURNAME = "surname";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_PHONE = "phone";
    public static final String USER_RATE = "rate";
    public static final String LOCATION_ID = "id";
    public static final String LOCATION_LONGITUDE = "longitude";
    public static final String LOCATION_LATITUDE = "latitude";
    public static final String FAVORITE_PLACE_ID = "id";
    public static final String FAVORITE_PLACE_NAME = "name";
    public static final String FAVORITE_PLACE_USER_ID =  "user_id";
    public static final String FAVORITE_PLACE_LOCATION_ID = "location_id";
    public static final String ORDER_ID = "id";
    public static final String ORDER_DRIVER_RATE = "driver_rate";
    public static final String ORDER_DISTANCE = "distance";
    public static final String ORDER_DATETIME = "datetime";
    public static final String ORDER_PRICE = "price";
    public static final String ORDER_DEPARTURE_LOCATION_ID = "departure_location_id";
    public static final String ORDER_DESTINATION_LOCATION_ID = "destination_location_id";
}
