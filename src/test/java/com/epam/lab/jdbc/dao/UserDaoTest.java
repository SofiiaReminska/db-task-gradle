package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoTest {

    private static final String INSERT_INTO_USER_QUERY = "INSERT INTO user(name,surname,email,password,phone,rate) VALUES (?,?,?,?,?,?)";

    @InjectMocks
    private UserDao unit;

    @Mock
    private Connection connection;
    @Mock
    private PreparedStatement statement;

    private User dummyUser;

    {
        dummyUser = new User.UserBuilder()
                .setName("Dummy name")
                .setSurname("Dummy surname")
                .setPassword("pass")
                .setPhone("123")
                .setEmail("dummy@dummy.com")
                .build();
        ;
    }

    @Test
    public void createTest() throws SQLException {
        when(connection.prepareStatement(INSERT_INTO_USER_QUERY)).thenReturn(statement);
        when(statement.executeUpdate()).thenReturn(1);
        Assert.assertEquals(1, unit.create(dummyUser));
    }
}
