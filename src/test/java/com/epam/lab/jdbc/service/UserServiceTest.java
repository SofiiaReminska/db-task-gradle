package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.dao.UserDao;
import com.epam.lab.jdbc.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService unit;

    @Mock
    private UserDao userDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createSampleUserTest() throws SQLException {
        when(userDao.create(any(User.class))).thenReturn(1);
        unit.createSampleUser();
        verify(userDao, times(1)).create(any(User.class));
    }
}
